# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3


class InputException(Exception):
    """Exception to indicate that the user has entered invalid input"""

Welcome to Portmod's documentation!
===================================

Portmod is a cross-platform cli package manager for mods. Based on Gentoo's `Portage <https://wiki.gentoo.org/wiki/Portage>`__ package manager.

Portmod is designed to be a powerful tool that allows mod installation information to be shared among a community and makes mod installation reproduceable, allowing users to spend less time worring about configuring and installing mods and more time discovering mods and playing games.

It supports multiple game engines through a modular python-based system, sandboxed for security.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install/index
   setup
   basic-usage
   cli/index
   concepts/index
   config/index
   contributing
   dev/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
